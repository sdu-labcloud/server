# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.33.6]

### Fixed

- Lint files correctly

## [0.33.5] - 2020-12-02

### Fixed

- Fix path for download refs

## [0.33.4] - 2020-12-02

### Fixed

- Builds on old branches don't need to be re-run to be available as updates via the application endpoint

## [0.33.3] - 2020-12-02

### Fixed

- Undo breaking changes

## [0.33.2] - 2020-12-02

### Fixed

- Builds on old branches don't need to be re-run to be available as updates via the application endpoint

## [0.33.1] - 2020-11-11

### Fixed

- Implement cache to prevent bombarding GitLab API

## [0.33.0] - 2020-11-10

### Added

- Endpoints for `telemetry-events`

## [0.32.4] - 2020-10-30

### Fixed

- Revert API to be RESTful again

## [0.32.3] - 2020-10-30

### Fixed

- Temporarily force nodes to certain update endpoint

## [0.32.2] - 2020-10-30

### Fixed

- Allow access to `PATCH /nodes/:id` endpoint for authorized nodes

## [0.32.1] - 2020-10-30

### Fixed

- Supply ID to reenable auto-updates

## [0.32.0] - 2020-10-30

### Added

- Query parameter to select application release channel

### Removed

- Endpoint to list all application versions

## [0.31.0] - 2020-10-30

### Added

- Add new fields to node
  - `release_channel`
  - `application_name`
  - `application_version`

## [0.30.3] - 2020-09-14

### Fixed

- Sorting of application versions such that it returns the latest version

## [0.30.2] - 2020-09-14

### Fixed

- Manual integration with GitLab API via `axios` instead of `@gitbeaker/node`

## [0.30.1] - 2020-09-11

### Fixed

- GitLab API calls

## [0.30.0] - 2020-09-11

### Added

- Endpoint to list `node` application versions
- Endpoint to get most recent `node` application version

### Fixed

- Fix unhandled promise rejections due to bad connection logic
- Feedback endpoint

## [0.29.2] - 2020-09-09

### Fixed

- If a node is deleted, all its sessions are also deleted

## [0.29.1] - 2020-09-04

### Fixed

- Allow to set `machine.user` in subsequent request after setting `machine.status` to `busy`

## [0.29.0] - 2020-09-01

### Added

- Reference to current `user` in `machine` model

## [0.28.2] - 2020-09-01

### Fixed

- When a machine is created it will have a link to the corresponding node

## [0.28.1] - 2020-09-01

### Fixed

- Remove `machine` reference in `node` if `machine` is deleted

## [0.28.0] - 2020-08-26

### Added

- New scheduling type `locking`
- Endpoint `DELETE /v1/nodes/:nodeId` to delete a single node

### Removed

- OS `micropython` is no longer valid for a node

## [0.27.9] - 2019-11-15

### Fixed

- Updated dependencies

## [0.27.8] - 2019-10-20

### Added

- Model for a manufacturing `job`
- Endpoint `GET /v1/jobs` to retrieve all jobs
- Query parameter `status` to search for job of a specific status only
- Endpoint `POST /v1/jobs` to create a single job
- Endpoint `GET /v1/jobs/:jobId` to retrieve a single job
- Endpoint `PATCH /v1/jobs/:jobId` to update a single job
- Endpoint `DELETE /v1/jobs/:jobId` to delete a single job

## [0.27.7] - 2019-09-19

### Fixed

- Fix connection to Docker daemon

## [0.27.6] - 2019-09-19

### Fixed

- Configure TLS certificates for Docker in Docker CI build

### Added

- Configure storage driver to be `overlay2` for increased performance

## [0.27.5] - 2019-09-19

### Added

- Endpoint `POST /v1/attachments/:attachmentId` to create a generic attachment
- Endpoint `GET /v1/attachments/:attachmentId` to read the metadata of a generic attachment
- Endpoint `DELETE /v1/attachments/:attachmentId` to delete a generic attachment
- Endpoint `GET /v1/attachments/:attachmentId/content` to receive the content of a generic attachment

## [0.27.4] - 2019-08-29

### Fixed

- Updated dependencies

## [0.27.3] - 2019-06-25

### Fixed

- Reduce database calls in identity endpoint `GET /v1/me`

## [0.27.2] - 2019-06-22

### Changed

- Endpoint `GET /v1/me` will now return either the `user` or the `node` object of the corresponding JWT alongside the name of the model

## [0.27.1] - 2019-05-19

### Fixed

- Endpoint `GET /v1/tools/:toolId` returns correct error message now if tool cannot be found

## [0.27.0] - 2019-05-14

### Added

- Endpoint `PATCH /v1/nodes/:nodeId` to update nodes
- Node model has two additional fields, `roles` and `capabilities`

## [0.26.7] - 2019-05-13

### Fixed

- Authorizing a session will also clear the node's references to old sessions
- Wrong call to database, which would not check the node's existence

## [0.26.6] - 2019-05-13

### Changed

- Authorizing a session will now remove all other sessions

## [0.26.5] - 2019-05-13

### Changed

- Rebrand application to LabCloud

### Fixed

- Allow more operating systems

## [0.26.4] - 2019-05-05

### Fixed

- Check for `type` in endpoint `PATCH /v1/tools/:toolId` to set `available` accordingly

## [0.26.3] - 2019-05-05

### Fixed

- Business logic in endpoint `PATCH /v1/tools/:toolId` now correctly assigned the values for `available` and `borrowed_by` based on the `type`

## [0.26.2] - 2019-05-04

### Fixed

- Forces uppercase letters for the `tag_id` attribute of tools

## [0.26.1] - 2019-05-03

### Added

- Endpoint `GET /v1/tool_types/schema` to read the tool type schema
- Endpoint `GET /v1/tools/schema` to read the tool schema
- Endpoint `GET /v1/tools` to get multiple tools
- Endpoint `POST /v1/tools` to create a new tool
- Endpoint `GET /v1/tools/:toolId`, which also supports the `tag_id` as identifier to read a single tool
- Endpoint `PATCH /v1/tools/:toolId`, which also supports the `tag_id` as identifier to update a single tool
- Endpoint `DELETE /v1/tools/:toolId` to delete a single tool type

## [0.26.0] - 2019-05-03

### Fixed

- **[BREAKING]** `card_id` attribute of users now contains a 4 byte hexadecimal string, which is the native representation of the RFID tag id on the card

## [0.25.11] - 2019-04-30

### Fixed

- Access control on tool type endpoints

## [0.25.10] - 2019-04-30

### Added

- Endpoint `GET /v1/tool_types` to get multiple tool types
- Endpoint `POST /v1/tool_types` to create a new tool type
- Endpoint `GET /v1/tool_types/:toolTypeId` to read a single tool type
- Endpoint `PATCH /v1/tool_types/:toolTypeId` to update a single tool type
- Endpoint `DELETE /v1/tool_types/:toolTypeId` to delete a single tool type

### Changed

- CI pipeline will pull docker image before stopping and removing old container to reduce downtime
- Some errors have and soon all will transition to a new format, which includes the error type and a message
  ```json
  {
    "error": {
      "type": "BodyValidationError",
      "message": "The field test is required and may not be empty."
    }
  }
  ```

### Fixed

- Permission endpoints will wait for image query to finish before evaluating if it exists

## [0.25.9] - 2019-04-28

### Fixed

- If a permission is deleted, it will also be removed from a user to prevent dangling references

## [0.25.8] - 2019-04-23

### Changed

- Endpoint `GET /v1/images/:imageId` moved to `GET /v1/images/:imageId/content` for better REST compliance
- Endpoint `GET /v1/images/:imageId/metadata` moved to `GET /v1/images/:imageId` for better REST compliance

## [0.25.7] - 2019-04-20

### Changed

- Permissions have an image attribute that stores the ObjectId of an image stored in the API
- Refactored the image service to expose mongoose-like interface

### Fixed

- Error message if abbreviation is not exactly three characters long

## [0.25.6] - 2019-04-20

### Added

- Endpoint `POST /v1/images` to create images with a maximum image size of 5MB
- Endpoint `GET /v1/images/:imageId` to read a single image
- Endpoint `DELETE /v1/images/:imageId` to delete a single image
- Endpoint `GET /v1/images/:imageId/metadata` to read metadata of a single image

### Fixed

- Two bugs in permission creation controller that could cause an unknown error
- Various wrong error messages and HTTP status codes if entities with a given ObjectId cannot be found

## [0.25.5] - 2019-01-22

### Fixed

- Node will only have machine removed if node link is edited on machine

## [0.25.4] - 2019-01-22

### Fixed

- Check if node is assigned to machine casts `ObjectId` to `String` now

## [0.25.3] - 2019-01-21

### Fixed

- Aligned node schema to make machine not required

## [0.25.2] - 2019-01-21

### Fixed

- Endpoint `POST /v1/nodes` is referencing correct operating systems for validation now

## [0.25.1] - 2019-01-21

### Fixed

- Session tokens include the session ID and the node ID
- Endpoint `GET /v1/users/:userId` is available to all authorized nodes
- Endpoint `PATCH /v1/machines/:machineId` is available to all authorized nodes assigned to the machine

## [0.25.0] - 2019-01-21

### Added

- Endpoint `GET /v1/machines/schema` to read machine database schema
- Endpoint `GET /v1/machines` to read multiple machines
- Endpoint `POST /v1/machines` to create a new machine
- Endpoint `GET /v1/machines/:machineId` to read a single machine
- Endpoint `PATCH /v1/machines/:machineId` to update a single machine
- Endpoint `DELETE /v1/machines/:machineId` to delete an existing machine
- Endpoint `GET /v1/machines/new` to obtain an available number

## [0.24.0] - 2019-01-15

### Added

- Endpoint `GET /v1/nodes/schema` to read node database schema
- Endpoint `GET /v1/sessions/schema` to read session database schema
- Endpoint `GET /v1/users/schema` to read user database schema

### Fixed

- Default permissions are now applied when a user first registers
- Default permissions are assigned to all know users upon creation

## [0.23.1] - 2019-01-15

### Fixed

- Serial numbers for nodes need to be hexadecimal and will be converted to uppercase

## [0.23.0] - 2018-12-29

### Added

- Endpoint `GET /v1/permissions/schema` to read permission database schema
- Endpoint `GET /v1/permissions/:permissionId` to read a single permission
- Endpoint `GET /v1/permissions` to read multiple permissions
- Endpoint `PATCH /v1/permissions/:permissionId` to update a single permissions
- Endpoint `DELETE /v1/permissions/:permissionId` to delete a single permissions

### Changed

- Database model of `permission` to account for different `workflow` and `scheduling` methods

## [0.22.1] - 2018-12-16

### Fixed

- Access to `POST /v1/permissions` is permitted for users with role `admin`

## [0.22.0] - 2018-12-16

### Changed

- Several middlewares have been refactored to be more useful
- Service definitions use `express` via the middleware pattern
- Adjustments to user model to ensure compatibility with dynamic permissions

### Added

- Endpoint `POST /v1/permissions` to create permission machine group

## [0.21.1] - 2018-12-14

### Fixed

- Endpoint `GET /v1/nodes/:nodeId/sessions/:sessionId` has correct URL now

## [0.21.0] - 2018-12-14

### Added

- Endpoint `GET /v1/nodes/:nodeId/sessions/:sessionId` for reading a single node session

### Fixed

- Endpoint `PATCH /v1/nodes/:nodeId/sessions/:sessionId` will return accurate `identify` property after setting `authorized` to `true`

## [0.20.2] - 2018-12-11

### Changed

- Only a single session per node can be identified at once

## [0.20.1] - 2018-12-10

### Fixed

- `identify` property is set to false now if a new session is authorized
- `machine` model exports correct model now (#21)

## [0.20.0] - 2018-12-07

### Fixed

- Endpoint `PATCH /v1/users/:userId` will respond with error code `404` and appropiate message if the entity is not found
- Endpoint `PATCH /v1/users/:userId` will check if the ID parameter is valid
- Endpoint `GET /v1/nodes/:nodeId` and endpoint `GET /v1/nodes` will return a valid response when an invalid population parameter is supplied

### Added

- Endpoint `GET /v1/nodes/:nodeId` and endpoint `GET /v1/nodes` support query parameter `?populate=sessions`
- Endpoint `PATCH /v1/nodes/:nodeId/sessions/:sessionId` to edit an existing session

### Removed

- Endpoint `GET /v1/nodes/:nodeId` does not support to populate the `session` anymore
- Property `session` of `node` model has been removed

## [0.19.0] - 2018-12-07

### Added

- `?populate=session,sessions` query parameter for the endpoint `GET /v1/nodes/:nodeId`
- Endpoint `POST /v1/nodes/:nodeId/sessions` to create a new session

## [0.18.1] - 2018-12-04

### Changed

- Change redirect url pathname to redirect to `/dashboard`

## [0.18.0] - 2018-12-04

### Added

- Part model for manufacturing files (CAD / CAM data)

## [0.17.0] - 2018-12-04

### Added

- Permission model for machine groups
- Machine model for assignment of existing assets

## [0.16.0] - 2018-12-01

### Added

- Endpoint `GET /v1/nodes/:nodeId` for getting a single IoT node with role `admin` or `staff`

## [0.15.0] - 2018-12-01

### Added

- Endpoint `GET /v1/nodes` for a list of all IoT nodes

## [0.14.1] - 2018-11-28

### Removed

- Endpoint `POST /v1/auth/azure_ad/hook` for Azure OpenID Connect callback has been superseeded by `POST /v1/auth/azure_ad`

## [0.14.0] - 2018-11-27

### Security

- Update dependencies to fix [event-stream vulnerability](https://github.com/dominictarr/event-stream/issues/116)

## [0.13.0] - 2018-11-26

### Added

- Endpoint `POST /v1/nodes` for registering a new IoT node

## [0.12.0] - 2018-11-24

### Added

- Endpoint `POST /v1/auth/azure_ad` for Azure OpenID Connect callback

## [0.11.0] - 2018-11-24

### Added

- Endpoint `DELETE /v1/users/:id` to delete a single user by `id`

## [0.10.1] - 2018-11-23

### Fixed

- CI should fail if the version number in the `CHANGELOG.md` is not `Unreleased` and the git tag exists

## [0.10.0] - 2018-11-23

### Added

- Endpoint `GET /v1/users/:id` to get single user by either `id`, `card_id` or `email`

## [0.9.0] - 2018-11-20

### Added

- Integration of `card_id` from MS Graph API via custom attribute

## [0.8.0] - 2018-11-13

### Added

- Endpoint `PATCH /v1/users/:id` to edit a single user

### Changed

- `trim` middleware does not accept excluded fields any more
- `trim` middleware trims strings recursively

## [0.7.0] - 2018-11-08

### Added

- Endpoint `GET /v1/users` for a list of users

## [0.6.1] - 2018-11-05

### Fixed

- Post-deployment test delay has been increased to `20s` to ensure a stable reverse proxy setup before the test (#1)

## [0.6.0] - 2018-11-05

### Added

- Card ID property `card_number` to `user` model
- New environment variables
  - `AZURE_AD_ATTRIBUTE_CARD_NUMBER`

## [0.5.1] - 2018-11-02

### Changed

- Change internal structure of `req.user` object to make it compatible with all middlewares

## [0.5.0] - 2018-10-31

### Added

- Endpoint `POST /v1/issues` for creation of user feedback
- New environment variables
  - `GITLAB_API_URL`
  - `GITLAB_API_TOKEN`
  - `GITLAB_PROJECT`

## [0.4.1] - 2018-10-28

### Fixed

- Added missing environment variable `JWT_KEY` to `Dockerfile` and CI pipeline

## [0.4.0] - 2018-10-28

### Fixed

- Timestamp plugin applies timestamps now correctly on update and on creation

### Added

- Update `README.md` with license information and environment variables
- Authentification with [passport](http://www.passportjs.org/) via [Azure](https://azure.microsoft.com/en-us/)
- Endpoint `GET /v1/me` for own identity query
- Endpoint `GET /v1/auth/azure_ad` for Azure OpenID Connect login
- Endpoint `POST /v1/auth/azure_ad/hook` for Azure OpenID Connect callback
- New environment variables
  - `APP_URL`
  - `AZURE_AD_CLIENT_ID`
  - `AZURE_AD_CLIENT_SECRET`
  - `AZURE_AD_TENANT`
  - `AZURE_AD_COOKIE_KEY_32`
  - `AZURE_AD_COOKIE_KEY_12`
  - `AZURE_AD_DOMAIN`

### Changed

- Refactored `src/common/env.js` module to apply default values to environment variables and fix tests

## [0.3.1] - 2018-10-12

### Fixed

- Add delay to wait for deployment to settle before post-deployment test

## [0.3.0] - 2018-10-12

### Added

- Connect deployment to environment in GitLab

### Changed

- Property `uptime` has been replaced by the property `started_at`, which returns the ISO 8601 string of the start time

## [0.2.0] - 2018-10-11

### Added

- Run linter in CI
- Install dependencies via `npm ci` to enforce aligned `package-lock.json`

### Changed

- Improved tagging mechanism checks if version in `CHANGELOG.md` is aligned with `package.json` and `package-lock.json`

### Fixed

- Align versions in `package.json` and `package-lock.json` with `CHANGELOG.md`

## [0.1.2] - 2018-10-11

### Fixed

- Typo in [.gitlab-ci.yml](./.gitlab-ci.yml) causing deployments to fail

## [0.1.1] - 2018-10-11

### Fixed

- Unit tests in CI
- Fix tag in Docker build

## [0.1.0] - 2018-10-10

### Added

- [Express](https://expressjs.com) setup for common REST server
- Environment variables via `.env`
- Code formatting with [Prettier](https://prettier.io)
- Linting with [ESLint](https://eslint.org/)
- Testing via [Jest](https://jestjs.io/)
- Mocking with `babel-plugin-rewire` and `babel-jest`
- Express testing with `supertest`
- [MIT License](https://mit-license.org/)
- CI with `.gitlab-ci.yml`
- `mongoose` as ODM for `mongodb`
- JSON response for unsupported endpoint
- JSON response for unknown error
- Internationalization with `i18n`
- Endpoint `GET /v1/health`
- Automatic tagging via [CHANGELOG.md](./CHANGELOG.md)
