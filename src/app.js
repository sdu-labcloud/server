const express = require('express');
const { loadSetupMiddlewares, loadErrorMiddlewares } = require('./middlewares');
const healthService = require('./services/health');
const authService = require('./services/auth');
const userService = require('./services/user');
const issueService = require('./services/issue');
const nodeService = require('./services/node');
const permissionService = require('./services/permission');
const machineService = require('./services/machine');
const imageService = require('./services/image');
const attachmentService = require('./services/attachment');
const toolTypeService = require('./services/toolType');
const toolService = require('./services/tool');
const jobService = require('./services/job');
const meService = require('./services/me');
const applicationService = require('./services/application');
const telemetryEventService = require('./services/telemetry-event');

const app = express();

exports.loadApp = () => {
  // load middlewares
  loadSetupMiddlewares(app);

  // load controllers
  app.use('/v1', healthService());
  app.use('/v1', authService());
  app.use('/v1', userService());
  app.use('/v1', issueService());
  app.use('/v1', nodeService());
  app.use('/v1', permissionService());
  app.use('/v1', machineService());
  app.use('/v1', imageService());
  app.use('/v1', toolTypeService());
  app.use('/v1', toolService());
  app.use('/v1', jobService());
  app.use('/v1', attachmentService());
  app.use('/v1', meService());
  app.use('/v1', applicationService());
  app.use('/v1', telemetryEventService());

  // load special middleware
  loadErrorMiddlewares(app);

  return app;
};
