const { trim, isString } = require('lodash');
const { walkValues } = require('../common/walk');

module.exports = (req, res, next) => {
  req.body = walkValues((str) => (isString(str) ? trim(str) : str))(req.body);
  return next();
};
