const { isUndefined } = require('lodash');

module.exports = (fields) => (req, res, next) => {
  const field = fields.find((prop) => isUndefined(req.body[prop]));
  if (field) {
    return res.status(400).json({
      error: res.tmf('validation_field_required', {
        field,
      }),
    });
  }

  return next();
};
