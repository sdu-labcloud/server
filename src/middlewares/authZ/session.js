const asyncMiddleware = require('express-async-handler');
const Session = require('../../services/session/model');

module.exports = (authMiddleware) =>
  asyncMiddleware(async (req, res, next) => {
    if (!authMiddleware) {
      const errorMessage = 'The authMiddleware must not be undefined or empty.';
      return next(new Error(errorMessage));
    }

    if (!req.user || !req.user.session || !req.user.session.id) {
      return authMiddleware(req, res, next);
    }

    if (!req.user.node || !req.user.node.id) {
      return authMiddleware(req, res, next);
    }

    const session = await Session.findById(req.user.session.id);

    if (!session || !session.authorized) {
      return authMiddleware(req, res, next);
    }

    if (session.node.toString() !== req.user.node.id) {
      return authMiddleware(req, res, next);
    }

    return next();
  });
