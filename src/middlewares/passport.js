const passport = require('passport');
const azureAdStrategy = require('../common/strategies/azureAd');
const jwtStrategy = require('../common/strategies/jwt');

passport.use('azureAd', azureAdStrategy);
passport.use('jwt', jwtStrategy);

module.exports = passport;
