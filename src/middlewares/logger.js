const logger = require('../common/logger');

module.exports = (req, res, next) => {
  const start = Date.now();
  res.on('finish', () => {
    const delta = Date.now() - start;
    const { statusCode } = res;
    const { method } = req;
    const baseUrl = req.baseUrl || '';
    const url = req.url ? req.url.split(/\?/).shift() || '' : '';
    logger.info(`[API] ${statusCode} ${method} ${baseUrl}${url} ${delta}ms`);
  });
  return next();
};
