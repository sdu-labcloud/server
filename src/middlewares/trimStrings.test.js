const express = require('express');
const supertest = require('supertest');
const bodyParser = require('body-parser');
const trimStrings = require('./trimStrings');

const echoEndpoint = (req, res) => res.status(200).json(req.body);

describe('trimStrings middleware', () => {
  it('removes trailing whitespaces and new lines', () => {
    return new Promise((done) => {
      // arrange
      const app = express()
        .use(bodyParser.json())
        .use(trimStrings)
        .post('/', echoEndpoint);
      const body = { test: 'Trim this! \n' };
      const status = 200;

      // act
      supertest(app)
        .post('/')
        .send(body)
        .end((err, res) => {
          // assert
          expect(res.body).toEqual({ test: 'Trim this!' });
          expect(res.status).toBe(status);

          done(err);
        });
    });
  });

  it('removes leading whitespaces and new lines', () => {
    return new Promise((done) => {
      // arrange
      const app = express()
        .use(bodyParser.json())
        .use(trimStrings)
        .post('/', echoEndpoint);
      const body = { test: '\n Trim this!' };
      const status = 200;

      // act
      supertest(app)
        .post('/')
        .send(body)
        .end((err, res) => {
          // assert
          expect(res.body).toEqual({ test: 'Trim this!' });
          expect(res.status).toBe(status);

          done(err);
        });
    });
  });

  it('trims recursively', () => {
    return new Promise((done) => {
      // arrange
      const app = express()
        .use(bodyParser.json())
        .use(trimStrings)
        .post('/', echoEndpoint);
      const body = {
        test: '\n Trim this!',
        notThis: {
          test: '\n Please trim me! \n',
        },
      };
      const status = 200;

      // act
      supertest(app)
        .post('/')
        .send(body)
        .end((err, res) => {
          // assert
          expect(res.body).toEqual({
            test: 'Trim this!',
            notThis: {
              test: 'Please trim me!',
            },
          });
          expect(res.status).toBe(status);

          done(err);
        });
    });
  });

  it('does not trim objects', () => {
    return new Promise((done) => {
      // arrange
      const app = express()
        .use(bodyParser.json())
        .use(trimStrings)
        .post('/', echoEndpoint);
      const body = {
        obj: {},
      };
      const status = 200;

      // act
      supertest(app)
        .post('/')
        .send(body)
        .end((err, res) => {
          // assert
          expect(res.body).toEqual({
            obj: {},
          });
          expect(res.status).toBe(status);

          done(err);
        });
    });
  });

  it('does not trim booleans', () => {
    return new Promise((done) => {
      // arrange
      const app = express()
        .use(bodyParser.json())
        .use(trimStrings)
        .post('/', echoEndpoint);
      const body = {
        boolTrue: true,
        boolFalse: false,
      };
      const status = 200;

      // act
      supertest(app)
        .post('/')
        .send(body)
        .end((err, res) => {
          // assert
          expect(res.body).toEqual({
            boolTrue: true,
            boolFalse: false,
          });
          expect(res.status).toBe(status);

          done(err);
        });
    });
  });

  it('does not trim null', () => {
    return new Promise((done) => {
      // arrange
      const app = express()
        .use(bodyParser.json())
        .use(trimStrings)
        .post('/', echoEndpoint);
      const body = {
        null: null,
      };
      const status = 200;

      // act
      supertest(app)
        .post('/')
        .send(body)
        .end((err, res) => {
          // assert
          expect(res.body).toEqual({
            null: null,
          });
          expect(res.status).toBe(status);

          done(err);
        });
    });
  });

  it('does not trim numbers', () => {
    return new Promise((done) => {
      // arrange
      const app = express()
        .use(bodyParser.json())
        .use(trimStrings)
        .post('/', echoEndpoint);
      const body = {
        numTruthy: 10,
        numFalsy: 0,
      };
      const status = 200;

      // act
      supertest(app)
        .post('/')
        .send(body)
        .end((err, res) => {
          // assert
          expect(res.body).toEqual({
            numTruthy: 10,
            numFalsy: 0,
          });
          expect(res.status).toBe(status);

          done(err);
        });
    });
  });
});
