const express = require('express');
const supertest = require('supertest');
const bodyParser = require('body-parser');
const requireFields = require('./requireFields');

const echoController = (req, res) => res.status(200).json(req.body);
const i18nMiddleware = (req, res, next) => {
  res.tmf = jest.fn((string) => string);
  res.t = jest.fn((string) => string);
  return next();
};

describe('requireFields middleware', () => {
  it('returns status code 400 and error message if body field is undefined', () => {
    return new Promise((done) => {
      // arrange
      const app = express()
        .use(bodyParser.json())
        .use(i18nMiddleware)
        .use(requireFields(['test']))
        .post('/', echoController);
      const body = {};
      const status = 400;

      // act
      supertest(app)
        .post('/')
        .send(body)
        .end((err, res) => {
          // assert
          expect(res.status).toBe(status);
          expect(res.body).toEqual({
            error: 'validation_field_required',
          });

          done(err);
        });
    });
  });

  it('calls next controller if body field is a string with content', () => {
    return new Promise((done) => {
      // arrange
      const app = express()
        .use(bodyParser.json())
        .use(i18nMiddleware)
        .use(requireFields(['test']))
        .post('/', echoController);
      const body = { test: 'a' };
      const status = 200;

      // act
      supertest(app)
        .post('/')
        .send(body)
        .end((err, res) => {
          // assert
          expect(res.status).toBe(status);
          expect(res.body).toEqual(body);

          done(err);
        });
    });
  });

  it('calls next controller if body field is an empty string', () => {
    return new Promise((done) => {
      // arrange
      const app = express()
        .use(bodyParser.json())
        .use(i18nMiddleware)
        .use(requireFields(['test']))
        .post('/', echoController);
      const body = { test: '' };
      const status = 200;

      // act
      supertest(app)
        .post('/')
        .send(body)
        .end((err, res) => {
          // assert
          expect(res.status).toBe(status);
          expect(res.body).toEqual(body);

          done(err);
        });
    });
  });

  it('calls next controller if body field is a number', () => {
    return new Promise((done) => {
      // arrange
      const app = express()
        .use(bodyParser.json())
        .use(i18nMiddleware)
        .use(requireFields(['test']))
        .post('/', echoController);
      const body = { test: 12 };
      const status = 200;

      // act
      supertest(app)
        .post('/')
        .send(body)
        .end((err, res) => {
          // assert
          expect(res.status).toBe(status);
          expect(res.body).toEqual(body);

          done(err);
        });
    });
  });

  it('calls next controller if body field is null', () => {
    return new Promise((done) => {
      // arrange
      const app = express()
        .use(bodyParser.json())
        .use(i18nMiddleware)
        .use(requireFields(['test']))
        .post('/', echoController);
      const body = { test: 12 };
      const status = 200;

      // act
      supertest(app)
        .post('/')
        .send(body)
        .end((err, res) => {
          // assert
          expect(res.status).toBe(status);
          expect(res.body).toEqual(body);

          done(err);
        });
    });
  });

  it('calls next controller if body field is an object', () => {
    return new Promise((done) => {
      // arrange
      const app = express()
        .use(bodyParser.json())
        .use(i18nMiddleware)
        .use(requireFields(['test']))
        .post('/', echoController);
      const body = { test: { some: 'data' } };
      const status = 200;

      // act
      supertest(app)
        .post('/')
        .send(body)
        .end((err, res) => {
          // assert
          expect(res.status).toBe(status);
          expect(res.body).toEqual(body);

          done(err);
        });
    });
  });

  it('calls next controller if body field is a boolean with true value', () => {
    return new Promise((done) => {
      // arrange
      const app = express()
        .use(bodyParser.json())
        .use(i18nMiddleware)
        .use(requireFields(['test']))
        .post('/', echoController);
      const body = { test: true };
      const status = 200;

      // act
      supertest(app)
        .post('/')
        .send(body)
        .end((err, res) => {
          // assert
          expect(res.status).toBe(status);
          expect(res.body).toEqual(body);

          done(err);
        });
    });
  });

  it('calls next controller if body field is a boolean with false value', () => {
    return new Promise((done) => {
      // arrange
      const app = express()
        .use(bodyParser.json())
        .use(i18nMiddleware)
        .use(requireFields(['test']))
        .post('/', echoController);
      const body = { test: false };
      const status = 200;

      // act
      supertest(app)
        .post('/')
        .send(body)
        .end((err, res) => {
          // assert
          expect(res.status).toBe(status);
          expect(res.body).toEqual(body);

          done(err);
        });
    });
  });
});
