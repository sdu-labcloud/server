const service = require('express').Router();
const { createOne } = require('./controllers');
const trimStrings = require('../../middlewares/trimStrings');
const filterFields = require('../../middlewares/filterFields');
const requireFields = require('../../middlewares/requireFields');
const jwtAuthN = require('../../middlewares/authN/jwt');

module.exports = () => {
  const modelFields = ['title', 'type', 'description'];

  // create one issue
  service.post('/issues', jwtAuthN);
  service.post('/issues', trimStrings);
  service.post('/issues', filterFields(modelFields));
  service.post('/issues', requireFields(modelFields));
  service.post('/issues', createOne);

  return service;
};
