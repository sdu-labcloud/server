const asyncMiddleware = require('express-async-handler');
const gitlab = require('../../common/gitlab');

const minimalLength = {
  title: 10,
  description: 20,
};
const types = ['kind/feature', 'kind/bug'];
const projectId = encodeURIComponent('sdu-labcloud/documentation');

exports.createOne = asyncMiddleware(async (req, res) => {
  if (req.body.title.length < minimalLength.title) {
    return res.status(400).json({
      error: res.tmf('validation_field_amount_min', {
        field: 'title',
        items: res.tn('%d character', minimalLength.title),
      }),
    });
  }

  if (req.body.description.length < minimalLength.description) {
    return res.status(400).json({
      error: res.tmf('validation_field_amount_min', {
        field: 'description',
        items: res.tn('%d character', minimalLength.description),
      }),
    });
  }

  if (!types.includes(req.body.type)) {
    return res.status(400).json({
      error: res.tmf('validation_field_value_out_of_range', {
        field: 'type',
        items: types.join(', '),
      }),
    });
  }

  const descriptionTemplate = [
    '##### User information',
    '\n',
    `**User:** ${req.user.user.display_name}  `,
    `**Email:** ${req.user.user.email}  `,
    '\n',
    '##### Description',
    '\n',
    req.body.description,
  ];
  const description = descriptionTemplate.join('\n');
  const labels = ['area/development', req.body.type, 'status/new'].join(',');

  const payload = {
    title: req.body.title,
    description,
    labels,
  };

  try {
    const endpoint = `/projects/${projectId}/issues`;
    const response = await gitlab.post(endpoint, payload);

    if (response.status !== 201) {
      return res.status(400).json({
        error: 'An unknown error occurred while creating the issue.',
      });
    }

    return res.status(201).json({
      data: {
        title: response.data.title,
        labels: response.data.labels,
        description: response.data.description,
        link: response.data.web_url,
      },
    });
  } catch (err) {
    return res.status(400).json({
      error: 'An unknown error occurred while creating the issue.',
    });
  }
});
