const service = require('express').Router();
const {
  readMany,
  readSchema,
  createOne,
  readOne,
  updateOne,
  deleteOne,
} = require('./controllers');
const requireFields = require('../../middlewares/requireFields');
const filterFields = require('../../middlewares/filterFields');
const jwtAuthN = require('../../middlewares/authN/jwt');

module.exports = () => {
  const modelFields = [
    'status',
    'attachments',
    'permission',
    'users',
    'supervisors',
    'description',
  ];
  const update = [...modelFields, 'time', 'started', 'machine'];

  // read many jobs
  service.get('/jobs', jwtAuthN);
  service.get('/jobs', readMany);

  // create one job
  service.post('/jobs', jwtAuthN);
  service.post('/jobs', requireFields(modelFields));
  service.post('/jobs', filterFields(update));
  service.post('/jobs', createOne);

  // read job schema
  service.get('/jobs/schema', readSchema);

  // read one job
  service.get('/jobs/:jobId', jwtAuthN);
  service.get('/jobs/:jobId', readOne);

  // update one machine
  service.patch('/jobs/:jobId', jwtAuthN);
  service.patch('/jobs/:jobId', filterFields(update));
  service.patch('/jobs/:jobId', updateOne);

  // delete one machine
  service.delete('/jobs/:jobId', jwtAuthN);
  service.delete('/jobs/:jobId', deleteOne);

  return service;
};
