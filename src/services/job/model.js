const mongoose = require('../../common/mongoose');
const generateMetaSchema = require('../../common/generateMetaSchema');

const status = ['pending', 'approved', 'active', 'expired', 'rejected'];
const defaultStatus = status[0];
const sortable = ['status', 'description', 'permission', 'machine', 'created'];
const populatable = ['permission', 'machine'];

const schema = new mongoose.Schema({
  status: {
    type: String,
    enum: status,
    default: defaultStatus,
    required: true,
  },
  attachments: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'attachments.file',
      required: true,
    },
  ],
  permission: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'permission',
    required: true,
  },
  users: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user',
      required: true,
    },
  ],
  supervisors: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user',
      required: true,
    },
  ],
  machine: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'machine',
  },
  started: {
    type: Date,
    default: null,
  },
  description: {
    type: String,
    required: false,
    default: '',
  },
  time: {
    type: Number,
    required: true,
    default: 4 * 60 * 60,
  },
});

schema.statics.status = status;
schema.statics.populatable = populatable;
schema.statics.sortable = sortable;
schema.statics.meta = generateMetaSchema(schema);

module.exports = mongoose.model('jobs', schema);
