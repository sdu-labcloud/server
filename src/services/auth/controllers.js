const asyncMiddleware = require('express-async-handler');
const jwt = require('jsonwebtoken');
const User = require('../user/model');
const Permissions = require('../permission/model');
const {
  WEBAPP_URL,
  JWT_KEY,
  AZURE_AD_ATTRIBUTE_CARD_NUMBER,
  AZURE_AD_ATTRIBUTE_CARD_ID,
} = require('../../common/env');

function normalizeCardId(cardNumberString) {
  const cardNumber = parseInt(cardNumberString, 10);
  if (!cardNumber) return '';
  const reversedHexString = cardNumber.toString(16).toUpperCase();
  if (!reversedHexString) return '';
  const bytes = reversedHexString.match(/.{2}/g);
  if (!bytes.length) return '';
  return bytes.reverse().join('');
}

exports.upsertFromAzureAd = asyncMiddleware(async (req, res) => {
  if (!req.user || !req.user.user) {
    return res.status(500).json({
      error: res.tmf('auth_error_no_identity', {
        entity: 'user',
      }),
    });
  }

  const query = { azure_id: req.user.user.id };
  const options = { new: true, upsert: true };
  const userInfo = {
    azure_id: req.user.user.id,
    email: req.user.user.mail,
    first_name: req.user.user.givenName,
    last_name: req.user.user.surname,
    display_name: req.user.user.displayName,
    card_number: req.user.user[AZURE_AD_ATTRIBUTE_CARD_NUMBER],
    card_id: normalizeCardId(req.user.user[AZURE_AD_ATTRIBUTE_CARD_ID]),
  };

  // find existing user account
  const existingUser = await User.findOne(query);

  // set defaults
  if (!existingUser) {
    const defaultPermissions = await Permissions.getDefaults();
    userInfo.role = User.meta.role.default;
    userInfo.permissions = defaultPermissions;
  }

  // create new user or update existing user
  const user = await User.findOneAndUpdate(query, userInfo, options);

  // create new json web token that is valid for one day
  const token = jwt.sign({ user }, JWT_KEY, { expiresIn: 86400 });

  return res.redirect(`${WEBAPP_URL}/dashboard#token=${token}`);
});
