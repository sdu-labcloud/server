const service = require('express').Router();
const { upsertFromAzureAd } = require('./controllers');
const azureAdAuthN = require('../../middlewares/authN/azureAd');

module.exports = () => {
  // azure active directory user login start
  service.get('/auth/azure_ad', azureAdAuthN);

  // azure active directory user login callback hook
  service.post('/auth/azure_ad', azureAdAuthN);
  service.post('/auth/azure_ad', upsertFromAzureAd);

  return service;
};
