const service = require('express').Router();
const { health } = require('./controllers');

module.exports = () => {
  // read health
  service.get('/health', health);

  return service;
};
