const service = require('express').Router();
const {
  readSchema,
  readMany,
  createOne,
  readOne,
  updateOne,
  deleteOne,
} = require('./controllers');
const jwtAuthN = require('../../middlewares/authN/jwt');
const userRole = require('../../middlewares/authZ/userRole');
const trimStrings = require('../../middlewares/trimStrings');
const filterFields = require('../../middlewares/filterFields');
const requireFields = require('../../middlewares/requireFields');

const isAdmin = userRole(['admin']);

module.exports = () => {
  const requiredFields = ['abbreviation', 'name', 'image'];
  const allowedFields = [
    ...requiredFields,
    'description',
    'scheduling',
    'workflow',
    'default',
  ];

  // read permission schema
  service.get('/permissions/schema', readSchema);

  // read many permissions
  service.get('/permissions', readMany);

  // create one permission
  service.post('/permissions', jwtAuthN);
  service.post('/permissions', isAdmin);
  service.post('/permissions', trimStrings);
  service.post('/permissions', filterFields(allowedFields));
  service.post('/permissions', requireFields(requiredFields));
  service.post('/permissions', createOne);

  // read one permission
  service.get('/permissions/:permissionId', readOne);

  // update one permission
  service.patch('/permissions/:permissionId', jwtAuthN);
  service.patch('/permissions/:permissionId', isAdmin);
  service.patch('/permissions/:permissionId', filterFields(allowedFields));
  service.patch('/permissions/:permissionId', updateOne);

  // delete one permission
  service.delete('/permissions/:permissionId', jwtAuthN);
  service.delete('/permissions/:permissionId', isAdmin);
  service.delete('/permissions/:permissionId', deleteOne);

  return service;
};
