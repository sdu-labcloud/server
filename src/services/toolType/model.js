const mongoose = require('../../common/mongoose');
const generateMetaSchema = require('../../common/generateMetaSchema');

const { Schema } = mongoose;
const { ObjectId } = Schema.Types;

const populatable = [];
const sortable = ['created', 'updated', 'name', 'description'];

const toolTypeSchema = new Schema({
  name: {
    type: String,
    required: true,
    unique: true,
  },
  description: {
    type: String,
    required: false,
    unique: false,
    default: '',
  },
  image: {
    type: ObjectId,
    ref: 'images.file',
    required: true,
    unique: false,
  },
});

toolTypeSchema.statics.populatable = populatable;
toolTypeSchema.statics.sortable = sortable;
toolTypeSchema.statics.meta = generateMetaSchema(toolTypeSchema);

module.exports = mongoose.model('tool_type', toolTypeSchema);
