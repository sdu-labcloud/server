const service = require('express').Router();
const {
  readSchema,
  readMany,
  createOne,
  readOne,
  updateOne,
  deleteOne,
} = require('./controllers');
const jwtAuthN = require('../../middlewares/authN/jwt');
const userRole = require('../../middlewares/authZ/userRole');

const isManager = userRole(['staff', 'admin']);

module.exports = () => {
  // read tool type schema
  service.get('/tool_types/schema', readSchema);

  // read many tool type
  service.get('/tool_types', readMany);

  // create one tool type
  service.post('/tool_types', jwtAuthN);
  service.post('/tool_types', isManager);
  service.post('/tool_types', createOne);

  // read one tool type
  service.get('/tool_types/:toolTypeId', readOne);

  // update one tool type
  service.patch('/tool_types/:toolTypeId', jwtAuthN);
  service.patch('/tool_types/:toolTypeId', isManager);
  service.patch('/tool_types/:toolTypeId', updateOne);

  // delete one tool type
  service.delete('/tool_types/:toolTypeId', jwtAuthN);
  service.delete('/tool_types/:toolTypeId', isManager);
  service.delete('/tool_types/:toolTypeId', deleteOne);

  return service;
};
