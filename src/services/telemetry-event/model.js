const mongoose = require('../../common/mongoose');

const { Schema } = mongoose;

const eventTypes = [
  'update_ready',
  'update_done',
  'lock_close',
  'lock_open',
  'access_denied',
  'access_granted',
  'access_overwrite',
  'usage_start',
  'usage_end',
];

const schema = new Schema({
  timestamp: {
    type: Date,
    required: true,
  },
  event_type: {
    type: String,
    required: true,
    enum: eventTypes,
  },
  metadata: {
    type: Object,
    default: {},
  },
});

module.exports = mongoose.model('telemetry-event', schema);
