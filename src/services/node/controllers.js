const asyncMiddleware = require('express-async-handler');
const { isString, isUndefined, isBoolean } = require('lodash');
const { isMACAddress, isHexadecimal } = require('validator');
const { isMongoId } = require('validator');
const jwt = require('jsonwebtoken');
const Node = require('./model');
const Session = require('../session/model');
const Machine = require('../machine/model');
const { JWT_KEY } = require('../../common/env');

exports.readNodeSchema = asyncMiddleware(async (req, res) =>
  res.status(200).json({ data: Node.meta })
);

exports.readSessionSchema = asyncMiddleware(async (req, res) =>
  res.status(200).json({ data: Session.meta })
);

exports.createOneNode = asyncMiddleware(async (req, res) => {
  const allowedOperatingSystems = Node.meta.operating_system.enum;
  const serialNumber = req.body.serial_number || '';
  const macAddress = req.body.mac_address || '';
  const operatingSystem = req.body.operating_system || '';
  const releaseChannel = req.body.release_channel || '';
  const applicationVersion = req.body.application_version || '';
  const applicationName = req.body.application_name || '';

  if (!isString(serialNumber)) {
    return res.status(400).json({
      error: res.tmf('validation_field_type_invalid', {
        field: 'serial_number',
        type: 'string',
      }),
    });
  }

  if (!isHexadecimal(serialNumber)) {
    return res.status(400).json({
      error: res.tmf('validation_field_type_invalid', {
        field: 'serial_number',
        type: 'hexadecimal',
      }),
    });
  }

  if (!isString(macAddress)) {
    return res.status(400).json({
      error: res.tmf('validation_field_type_invalid', {
        field: 'mac_address',
        type: 'string',
      }),
    });
  }

  if (!isString(operatingSystem)) {
    return res.status(400).json({
      error: res.tmf('validation_field_type_invalid', {
        field: 'operating_system',
        type: 'string',
      }),
    });
  }

  if (!isMACAddress(macAddress, { no_colons: true })) {
    return res.status(400).json({
      error: res.tmf('validation_field_mac_address', {
        field: 'mac_address',
      }),
    });
  }

  if (!allowedOperatingSystems.includes(operatingSystem)) {
    return res.status(400).json({
      error: res.tmf('validation_field_value_out_of_range', {
        field: 'operating_system',
        items: allowedOperatingSystems.join(', '),
      }),
    });
  }

  const query = {
    serial_number: serialNumber.toUpperCase(),
    mac_address: macAddress.toUpperCase(),
  };
  const node = await Node.findOne(query);

  if (node) {
    return res.status(409).json({
      error: res.tmf('validation_entity_exists', {
        model: 'node',
        uniqueProperties: ['serial_number', 'mac_address'].join(', '),
      }),
      id: node.id,
    });
  }

  const newNode = await Node.create({
    serial_number: serialNumber.toUpperCase(),
    mac_address: macAddress.toUpperCase(),
    operating_system: operatingSystem,
    release_channel: releaseChannel,
    application_name: applicationName,
    application_version: applicationVersion,
  });

  return res.status(201).json({
    data: newNode,
  });
});

exports.readOneNode = asyncMiddleware(async (req, res) => {
  const { nodeId } = req.params;
  const populate = {
    path: '',
    select: '-token',
  };

  if (req.query.populate) {
    const fields = req.query.populate.split(',');
    if (fields.find((field) => !Node.populatable.includes(field))) {
      return res.status(400).json({
        error: res.tmf('validation_query_value_out_of_range_multiple', {
          field: 'populate',
          items: Node.populatable.join(','),
        }),
      });
    }
    populate.path = fields.join(' ');
  }

  if (!isMongoId(nodeId)) {
    return res.status(400).json({
      error: res.tmf('validation_param_type_invalid', {
        type: 'ObjectId',
      }),
    });
  }

  const node = await Node.findById(nodeId).populate(populate);
  if (!node) {
    return res.status(404).json({
      error: res.tmf('error_entity_not_found', {
        entity: 'node',
      }),
    });
  }

  return res.status(200).json({
    data: node,
  });
});

exports.updateOneNode = asyncMiddleware(async (req, res) => {
  const { nodeId } = req.params;
  const allowedRoles = Node.meta.roles[0].enum;
  const allowedCapabilities = Node.meta.capabilities[0].enum;
  const roles = req.body.roles || [];
  const capabilities = req.body.capabilities || [];

  if (!isMongoId(nodeId)) {
    return res.status(422).json({
      error: res.tmf('validation_param_type_invalid', {
        type: 'ObjectId',
      }),
    });
  }

  if (!Array.isArray(roles)) {
    return res.status(422).json({
      error: res.tmf('validation_field_type_invalid', {
        field: 'roles',
        type: 'array of strings',
      }),
    });
  }

  if (!Array.isArray(capabilities)) {
    return res.status(422).json({
      error: res.tmf('validation_field_type_invalid', {
        field: 'capabilities',
        type: 'array of strings',
      }),
    });
  }

  if (roles.find((item) => !allowedRoles.includes(item))) {
    return res.status(422).json({
      error: res.tmf('validation_field_value_out_of_range_multiple', {
        field: 'roles',
        type: 'strings',
        items: allowedRoles.join(','),
      }),
    });
  }

  if (capabilities.find((item) => !allowedCapabilities.includes(item))) {
    return res.status(422).json({
      error: res.tmf('validation_field_value_out_of_range_multiple', {
        field: 'capabilities',
        type: 'strings',
        items: allowedRoles.join(','),
      }),
    });
  }

  const update = req.body;
  const options = { new: true };
  const node = await Node.findByIdAndUpdate(nodeId, update, options);
  if (!node) {
    return res.status(404).json({
      error: res.tmf('error_entity_not_found', {
        entity: 'node',
      }),
    });
  }

  return res.status(200).json({
    data: node,
  });
});

exports.readManyNodes = asyncMiddleware(async (req, res) => {
  const limit = parseInt(req.query.limit, 10) || 100;
  const offset = parseInt(req.query.offset, 10) || 0;
  const sortOrder = req.query.sort_order || 'asc';
  const sortBy = req.query.sort_by || 'created';
  const allowedSortOrder = ['asc', 'desc'];
  const allowedSortBy = ['created', 'updated'];

  if (sortBy && Array.isArray(sortBy)) {
    return res.status(400).json({
      error: res.tmf('validation_query_field_not_unique', {
        field: 'sort_by',
      }),
    });
  }

  if (sortOrder && Array.isArray(sortOrder)) {
    return res.status(400).json({
      error: res.tmf('validation_query_field_not_unique', {
        field: 'sort_order',
      }),
    });
  }

  if (sortOrder && !allowedSortOrder.includes(sortOrder)) {
    return res.status(400).json({
      error: res.tmf('validation_query_value_out_of_range', {
        field: 'sort_order',
        items: allowedSortOrder.join(', '),
      }),
    });
  }

  if (sortBy && !allowedSortBy.includes(sortBy)) {
    return res.status(400).json({
      error: res.tmf('validation_query_value_out_of_range', {
        field: 'sort_by',
        items: allowedSortBy.join(', '),
      }),
    });
  }

  const populate = {
    path: '',
    select: '-token',
  };
  if (req.query.populate) {
    const fields = req.query.populate.split(',');
    if (fields.find((field) => !Node.populatable.includes(field))) {
      return res.status(400).json({
        error: res.tmf('validation_query_value_out_of_range_multiple', {
          field: 'populate',
          items: Node.populatable.join(','),
        }),
      });
    }
    populate.path = fields.join(' ');
  }

  const options = { sort: { [sortBy]: sortOrder }, offset, limit, populate };
  const { docs, totalDocs, ...metadata } = await Node.paginate({}, options);
  return res.status(200).json({
    data: docs,
    count: totalDocs,
    ...metadata,
  });
});

exports.deleteOneNode = asyncMiddleware(async (req, res) => {
  const { nodeId } = req.params;

  if (!isMongoId(nodeId)) {
    return res.status(422).json({
      error: res.tmf('validation_param_type_invalid', {
        type: 'ObjectId',
      }),
    });
  }

  const node = await Node.findByIdAndDelete(nodeId);
  if (!node) {
    return res.status(404).json({
      error: res.tmf('error_entity_not_found', {
        entity: 'node',
      }),
    });
  }

  if (node.machine) {
    await Machine.findByIdAndUpdate(node.machine, { node: null });
  }

  await Session.deleteMany({ _id: { $in: node.sessions } });

  return res.status(204).json(null);
});

exports.readOneNodeSession = asyncMiddleware(async (req, res) => {
  const { nodeId, sessionId } = req.params;

  const node = await Node.findById(nodeId);
  if (!node) {
    return res.status(404).json({
      error: res.tmf('error_entity_not_found', {
        entity: 'node',
      }),
    });
  }

  const query = { _id: sessionId, node: nodeId };
  const session = await Session.findOne(query, '-token');
  if (!session) {
    return res.status(404).json({
      error: res.tmf('error_entity_not_found', {
        entity: 'session',
      }),
    });
  }

  return res.status(200).json({
    data: session,
  });
});

exports.createOneNodeSession = asyncMiddleware(async (req, res) => {
  const { nodeId } = req.params;

  const node = await Node.findById(nodeId);
  if (!node) {
    return res.status(404).json({
      error: res.tmf('error_entity_not_found', {
        entity: 'node',
      }),
    });
  }

  const nodeSession = await Session.create({ node: nodeId });

  const payload = { node: { id: node.id }, session: { id: nodeSession.id } };
  const token = jwt.sign(payload, JWT_KEY);

  nodeSession.token = token;
  await nodeSession.save();

  node.sessions.push(nodeSession.id);
  await node.save();

  return res.status(201).json({
    data: nodeSession,
  });
});

exports.patchOneNodeSession = asyncMiddleware(async (req, res) => {
  const { nodeId, sessionId } = req.params;

  if (!isMongoId(nodeId)) {
    return res.status(400).json({
      error: res.tmf('validation_param_type_invalid', {
        type: 'ObjectId',
      }),
    });
  }

  if (!isMongoId(sessionId)) {
    return res.status(400).json({
      error: res.tmf('validation_param_type_invalid', {
        type: 'ObjectId',
      }),
    });
  }

  const node = await Node.findById(nodeId);
  if (!node) {
    return res.status(404).json({
      error: res.tmf('error_entity_not_found', {
        entity: 'node',
      }),
    });
  }

  const { authorized } = req.body;
  if (!isUndefined(authorized) && !isBoolean(authorized)) {
    return res.status(400).json({
      error: res.tmf('validation_field_type_invalid', {
        field: 'authorized',
        type: 'boolean',
      }),
    });
  }

  const { identify } = req.body;
  if (!isUndefined(identify) && !isBoolean(identify)) {
    return res.status(400).json({
      error: res.tmf('validation_field_type_invalid', {
        field: 'identify',
        type: 'boolean',
      }),
    });
  }
  const update = req.body;
  const options = { new: true, select: '-token' };
  const session = await Session.findByIdAndUpdate(sessionId, update, options);

  if (!session) {
    return res.status(404).json({
      error: res.tmf('error_entity_not_found', {
        entity: 'session',
      }),
    });
  }

  if (authorized) {
    // only one session can be authorized
    // all other sessions can be discarded
    await session.deleteOther();
    // update current session
    session.identify = false;
    await session.save();
    // update node
    node.sessions = [session.id];
    await node.save();
  }
  if (identify) {
    // only one session should be identified at once
    await session.unidentifyOther();
  }

  return res.status(200).json({
    data: session,
  });
});
