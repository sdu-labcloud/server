const service = require('express').Router();
const {
  readNew,
  readMany,
  readOne,
  createOne,
  updateOne,
  deleteOne,
  readSchema,
} = require('./controllers');
const requireFields = require('../../middlewares/requireFields');
const filterFields = require('../../middlewares/filterFields');
const jwtAuthN = require('../../middlewares/authN/jwt');
const userRole = require('../../middlewares/authZ/userRole');
const session = require('../../middlewares/authZ/session');

const isManager = userRole(['staff', 'admin']);

module.exports = () => {
  const modelFields = ['permission'];
  const update = [...modelFields, 'status', 'node', 'user'];

  // read many machines
  service.get('/machines', readMany);

  // create one machine
  service.post('/machines', jwtAuthN);
  service.post('/machines', isManager);
  service.post('/machines', requireFields(modelFields));
  service.post('/machines', filterFields(update));
  service.post('/machines', createOne);

  // read new machine number
  service.get('/machines/new', jwtAuthN);
  service.get('/machines/new', isManager);
  service.get('/machines/new', readNew);

  // read machine schema
  service.get('/machines/schema', readSchema);

  // read one machine
  service.get('/machines/:machineId', readOne);

  // update one machine
  service.patch('/machines/:machineId', jwtAuthN);
  service.patch('/machines/:machineId', session(isManager));
  service.patch('/machines/:machineId', filterFields(update));
  service.patch('/machines/:machineId', updateOne);

  // delete one machine
  service.delete('/machines/:machineId', jwtAuthN);
  service.delete('/machines/:machineId', isManager);
  service.delete('/machines/:machineId', deleteOne);

  return service;
};
