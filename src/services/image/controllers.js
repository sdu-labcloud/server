const asyncMiddleware = require('express-async-handler');
const Busboy = require('busboy');
const { get } = require('lodash');
const { isMongoId } = require('validator');
const Image = require('./model');

const getErrorType = (err) => err.message.split(':').shift();

exports.createOne = asyncMiddleware(async (req, res) => {
  const busboy = new Busboy({
    headers: req.headers,
    limits: {
      // limit file amount
      files: 1,
      // limit filesize to 5MB
      fileSize: 5000000,
    },
  });
  let writeStream = null;
  let limitExceeded = false;
  let field = '';

  // process incoming files
  busboy.on(
    'file',
    async (fieldname, readStream, filename, encoding, mimeType) => {
      field = fieldname;

      // check for maximum file size limit
      readStream.on('limit', () => {
        limitExceeded = true;
      });

      // open write stream to GridFS
      writeStream = await Image.create(filename, {
        contentType: mimeType,
      });

      readStream.pipe(writeStream);
    }
  );

  // wait for transmission to end
  busboy.on('finish', () => {
    if (field !== 'image') {
      return res.status(422).json({
        error: res.tmf('validation_field_required', {
          field: 'image',
        }),
      });
    }

    return writeStream.on('finish', async (file) => {
      const objectId = get(file, '_id');
      if (limitExceeded) {
        await Image.findByIdAndDelete(objectId);
        return res.status(413).json({
          error: res.tmf('validation_upload_limit_exceeded', {
            size: '5MB',
          }),
        });
      }

      return res.status(200).json({
        data: {
          created: file.uploadDate.toISOString(),
          mime_type: file.contentType,
          filename: file.filename,
          id: objectId.toString(),
        },
      });
    });
  });

  req.pipe(busboy);
});

exports.readOne = asyncMiddleware(async (req, res) => {
  const { imageId } = req.params;

  if (!isMongoId(imageId)) {
    return res.status(400).json({
      error: res.tmf('validation_param_type_invalid', {
        type: 'ObjectId',
      }),
    });
  }

  const file = await Image.findById(imageId);
  if (!file) {
    return res.status(404).json({
      error: res.tmf('error_entity_not_found', {
        entity: 'file',
      }),
    });
  }

  return res.status(200).json({
    data: {
      created: file.uploadDate.toISOString(),
      mime_type: file.contentType,
      filename: file.filename,
      id: file.id,
    },
  });
});

exports.readOneContent = asyncMiddleware(async (req, res) => {
  const { imageId } = req.params;

  if (!isMongoId(imageId)) {
    return res.status(400).json({
      error: res.tmf('validation_param_type_invalid', {
        type: 'ObjectId',
      }),
    });
  }

  const file = await Image.findById(imageId);
  if (!file) {
    return res.status(404).json({
      error: res.tmf('error_entity_not_found', {
        entity: 'file',
      }),
    });
  }

  const readStream = await Image.findByIdAndRead(imageId);
  return readStream.pipe(res.status(200).set('Content-Type', file.contentType));
});

exports.deleteOne = asyncMiddleware(async (req, res) => {
  const { imageId } = req.params;

  if (!isMongoId(imageId)) {
    return res.status(400).json({
      error: res.tmf('validation_param_type_invalid', {
        type: 'ObjectId',
      }),
    });
  }

  try {
    await Image.findByIdAndDelete(imageId);
    return res.status(204).json(null);
  } catch (err) {
    if (getErrorType(err) !== 'FileNotFound') {
      throw err;
    }
    return res.status(404).json({
      error: res.tmf('error_entity_not_found', {
        entity: 'file',
      }),
    });
  }
});
