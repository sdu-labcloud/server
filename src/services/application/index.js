const service = require('express').Router();
const { readOne } = require('./controllers');

module.exports = () => {
  // Read a single application.
  service.get('/applications/:id', readOne);

  return service;
};
