const asyncMiddleware = require('express-async-handler');
const gitlab = require('../../common/gitlab');

function ApplicationCache() {
  this.cache = {};
  this.interval = 60000;

  this.get = function get(app, channel, arch) {
    const item = this.cache[`${app}-${channel}-${arch}`];

    // Cache item does not exist.
    if (!item) {
      return null;
    }

    // Cache item is expired.
    if (item.expiration < Date.now()) {
      delete this.cache[`${app}-${channel}-${arch}`];
      return null;
    }

    return item;
  };

  this.set = function set(app, channel, arch, data) {
    this.cache[`${app}-${channel}-${arch}`] = {
      data,
      expiration: Date.now() + this.interval,
    };
  };

  return this;
}

const cache = new ApplicationCache();

exports.readOne = asyncMiddleware(async (req, res) => {
  const app = req.params.id || '';
  const arch = req.query.cpu_architecture || 'arm';
  const releaseChannel = req.query.release_channel || 'master';
  const archs = ['arm', 'amd64'];

  if (!arch || !archs.includes(arch)) {
    return res.status(400).json({
      error: res.tmf('validation_query_value_out_of_range', {
        field: 'cpu_architecture',
        items: archs.join(', '),
      }),
    });
  }

  if (!app || app.substring(0, 4) !== 'node') {
    return res.status(404).json({
      error: res.tmf('error_entity_not_found', {
        entity: 'application',
      }),
    });
  }

  // Serve cache if possible.
  const cacheItem = cache.get(app, releaseChannel, arch);
  if (cacheItem) {
    return res.status(200).json({ data: cacheItem.data });
  }

  const projectId = encodeURIComponent(`sdu-labcloud/${app}`);

  // Fetch latest successful pipeline for branch.
  const pipelinesEndpoint = `/projects/${projectId}/pipelines?ref=${releaseChannel}&status=success&per_page=1`;
  const pipelineResponse = await gitlab.get(pipelinesEndpoint);
  const pipelineData = pipelineResponse.data || {};
  const pipelineId = pipelineData.length === 1 ? pipelineData[0].id : '';
  if (pipelineResponse.status !== 200 || !pipelineId) {
    return res.status(404).json({
      error: res.tmf('error_entity_not_found', {
        entity: 'application',
      }),
    });
  }

  // Get all jobs of the latest successful pipeline.
  const jobsEndpoint = `/projects/${projectId}/pipelines/${pipelineId}/jobs`;
  const jobsResponse = await gitlab.get(jobsEndpoint);
  const job = jobsResponse.data.find((j) => j.name === `compile-${arch}`);
  if (jobsResponse.status !== 200 || !job) {
    return res.status(404).json({
      error: res.tmf('error_entity_not_found', {
        entity: 'application',
      }),
    });
  }

  const downloadLink = `${gitlab.defaults.baseURL.slice(
    0,
    -7
  )}/sdu-labcloud/${app}/-/jobs/artifacts/${releaseChannel}/raw/${app}-${arch}?job=compile-${arch}`;
  const application = {
    application: app,
    commit_sha: job.commit.id,
    download_href: downloadLink,
    cpu_architecture: arch,
    release_channel: releaseChannel,
  };

  // Update cache.
  cache.set(app, releaseChannel, arch, application);

  return res.status(200).json({ data: application });
});
