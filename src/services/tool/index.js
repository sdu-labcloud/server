const service = require('express').Router();
const {
  readSchema,
  readMany,
  createOne,
  readOne,
  updateOne,
  deleteOne,
} = require('./controllers');
const jwtAuthN = require('../../middlewares/authN/jwt');
const session = require('../../middlewares/authZ/session');
const userRole = require('../../middlewares/authZ/userRole');

const isManager = userRole(['staff', 'admin']);

module.exports = () => {
  // read tool schema
  service.get('/tools/schema', readSchema);

  // read many tools
  service.get('/tools', readMany);

  // create one tool
  service.post('/tools', jwtAuthN);
  service.post('/tools', session(isManager));
  service.post('/tools', createOne);

  // read one tool
  service.get('/tools/:toolId', readOne);

  // update one tool
  service.patch('/tools/:toolId', jwtAuthN);
  service.patch('/tools/:toolId', session(isManager));
  service.patch('/tools/:toolId', updateOne);

  // delete one tool
  service.delete('/tools/:toolId', jwtAuthN);
  service.delete('/tools/:toolId', isManager);
  service.delete('/tools/:toolId', deleteOne);

  return service;
};
