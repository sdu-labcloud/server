const mongoose = require('../../common/mongoose');
const generateMetaSchema = require('../../common/generateMetaSchema');

const { Schema } = mongoose;
const { ObjectId } = Schema.Types;

const populatable = ['type', 'borrowed_by'];
const sortable = [
  'created',
  'type',
  'size',
  'shape',
  'borrowed_by',
  'borrowed_since',
  'available',
];

const toolSchema = new Schema({
  tag_id: {
    type: String,
    required: true,
    unique: true,
  },
  type: {
    type: ObjectId,
    ref: 'tool_type',
    required: false,
    default: null,
    unique: false,
  },
  size: {
    type: String,
    required: false,
    default: '',
    unique: false,
  },
  shape: {
    type: String,
    required: false,
    default: '',
    unique: false,
  },
  borrowed_by: {
    type: ObjectId,
    ref: 'user',
    required: false,
    default: null,
    unique: false,
  },
  borrowed_since: {
    type: Date,
    required: false,
    default: Date.now,
    unique: false,
  },
  available: {
    type: Boolean,
    required: true,
    default: true,
    unique: false,
  },
});

toolSchema.statics.populatable = populatable;
toolSchema.statics.sortable = sortable;
toolSchema.statics.meta = generateMetaSchema(toolSchema);

module.exports = mongoose.model('tool', toolSchema);
