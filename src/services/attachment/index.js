const service = require('express').Router();
const {
  createOne,
  readOne,
  deleteOne,
  readOneContent,
} = require('./controllers');
const jwtAuthN = require('../../middlewares/authN/jwt');

module.exports = () => {
  // create one image
  service.post('/attachments', jwtAuthN);
  service.post('/attachments', createOne);

  // read one image
  service.get('/attachments', jwtAuthN);
  service.get('/attachments/:attachmentId', readOne);

  // read metadata of one image
  service.get('/attachments', jwtAuthN);
  service.get('/attachments/:attachmentId/content', readOneContent);

  // delete one image
  service.delete('/attachments/:attachmentId', jwtAuthN);
  service.delete('/attachments/:attachmentId', deleteOne);

  return service;
};
