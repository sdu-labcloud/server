const mongoose = require('../../common/mongoose');
const generateMetaSchema = require('../../common/generateMetaSchema');

const { Schema } = mongoose;
const { ObjectId } = Schema.Types;
const roles = ['user', 'staff', 'supervisor', 'admin'];
const [defaultRole] = roles;

const userSchema = new Schema({
  azure_id: {
    type: String,
    required: true,
    unique: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  card_id: {
    type: String,
    required: true,
    unique: true,
  },
  card_number: {
    type: String,
    required: true,
    unique: false,
  },
  first_name: {
    type: String,
    required: true,
    unique: false,
  },
  last_name: {
    type: String,
    required: true,
    unique: false,
  },
  display_name: {
    type: String,
    required: true,
    unique: false,
  },
  role: {
    type: String,
    required: true,
    unique: false,
    enum: roles,
    default: defaultRole,
  },
  permissions: [
    {
      type: ObjectId,
      ref: 'permission',
      required: false,
      unique: false,
      default: [],
    },
  ],
});

userSchema.statics.meta = generateMetaSchema(userSchema);

module.exports = mongoose.model('user', userSchema);
