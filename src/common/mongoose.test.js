const mongoose = require('mongoose');

describe('mongoose', () => {
  it('exposes the mongoose interface', () => {
    // assert
    expect(mongoose.model).toBeInstanceOf(Function);
    expect(mongoose.Schema).toBeInstanceOf(Function);
    expect(mongoose.Schema.Types.ObjectId).toBeInstanceOf(Function);
  });
});
