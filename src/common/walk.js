const { isObjectLike, isArray, keys } = require('lodash');

exports.walkValues = (func) => (obj) => {
  if (isObjectLike(obj)) {
    if (isArray(obj)) {
      return obj.map(exports.walkValues(func));
    }
    return keys(obj).reduce((prev, key) => {
      const next = prev;
      const value = obj[key];
      if (isObjectLike(value)) {
        next[key] = exports.walkValues(func)(value);
      } else {
        next[key] = func(value);
      }
      return next;
    }, {});
  }
  return obj;
};
