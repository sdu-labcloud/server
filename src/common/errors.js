/* eslint-disable max-classes-per-file */
class HttpError extends Error {
  constructor(message, statusCode) {
    super(message);
    this.name = this.constructor.name;
    this.statusCode = statusCode;
  }
}

class ConflictError extends HttpError {
  constructor(message) {
    super(message, 409);
  }
}

class ValidationError extends HttpError {
  constructor(message) {
    super(message, 422);
  }
}

class BodyValidationError extends ValidationError {}
class QueryValidationError extends ValidationError {}
class ParameterValidationError extends ValidationError {}

class NotFoundError extends HttpError {
  constructor(message) {
    super(message, 404);
  }
}

class EntityNotFoundError extends NotFoundError {}

module.exports = {
  HttpError,
  ConflictError,
  ValidationError,
  BodyValidationError,
  QueryValidationError,
  ParameterValidationError,
  NotFoundError,
  EntityNotFoundError,
};
